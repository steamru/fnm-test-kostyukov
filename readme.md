Что нужно сделать:
1) В облаке гугла создать новый проект (для новых учеток есть 300 USD welcome limit)
2) В созданном проекте поднять кластер кубернетеса
3) Настроить через gitlab-ci выкладку приложения из папки src в поднятый кластер при коммите в ветку master

# Usage

1. `git clone https://gitlab.com/steamru/fnm-test-kostyukov.git`
1. Checkout a new branch from master, contribute and make a PR back to master branch
1. Merge into master, wait until the pipeline is green and access http://164.90.240.188 (Kubernetes endpoint at DitialOcean)

# Implementation

The C# code is built with [Dockerfile](src/TestApp/TestApp/Dockerfile) then the image is pushed back to Gitlab [container registry](https://gitlab.com/steamru/fnm-test-kostyukov/container_registry) (no image versioning, always the latest). If the phase succeeded then [Deployment](k8s/app-deployment.yaml) is done to pre-provisioned Kubernetes cluster at DigitalOcean, see also [Service file](k8s/app-service.yaml) for k8s setup.

Gitlab container registry is granted to maximfox.bot username, defined via Gitlab tokens and set in project vars.

Kubectl config is located in project DO_KUBE_CERT file type var.

DigitalOcean was chosen despite the reqs for GCP due to personal account limit (no more free tier available).


# Known issues

At 0.1-alpha release http://164.90.240.188 returns 404 not found. The cause is within the app or image setup, can be repeated without Kubernetes, just with Docker engine container. Run `docker run -d -p 8888:80 registry.gitlab.com/steamru/fnm-test-kostyukov` and point out the browser to http://localhost:8888 to probe the app.

# Troubleshooting

`kubectl get all` should output healthy statuses of the services and the pods:

```
NAME                           READY   STATUS    RESTARTS   AGE
pod/app-test-cc7f69656-bsvv8   1/1     Running   0          6h24m

NAME                 TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)        AGE
service/app-test     LoadBalancer   10.245.166.57   164.90.240.188   80:30645/TCP   6h24m
service/kubernetes   ClusterIP      10.245.0.1      <none>           443/TCP        6h55m

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/app-test   1/1     1            1           6h24m

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/app-test-cc7f69656   1         1         1       6h24m
```


